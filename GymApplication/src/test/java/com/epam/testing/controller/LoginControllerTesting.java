package com.epam.testing.controller;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.LoginController;
import com.epam.exception.LoginException;
import com.epam.service.LoginService;
import com.epam.service.LoginServiceImpl;

@WebMvcTest(LoginController.class)
public class LoginControllerTesting {
	
	@MockBean
	LoginServiceImpl loginService;
	
	@Autowired
	MockMvc mockMvc;
	
	
    @Test
    public void testLoginUserSuccess() throws Exception {
    	
        Mockito.doNothing().when(loginService).loginUser(Mockito.any(), Mockito.any());
    	
        mockMvc.perform(post("/gymapp/login")
                .param("username", "testUser")
                .param("password", "testPassword"))
                .andExpect(status().isOk());

        verify(loginService).loginUser("testUser", "testPassword");
    }
	
    
    @Test
    public void testLoginUserFailure() throws Exception {
        doThrow(new LoginException("Login failed")).when(loginService).loginUser(Mockito.any(), Mockito.any());

        mockMvc.perform(post("/gymapp/login")
                .param("username", "testUser")
                .param("password", "testPassword"))
                .andExpect(status().isBadRequest());
    }
	
    
    @Test
    public void testUpdatePasswordSuccess() throws Exception {
    	
        Mockito.doNothing().when(loginService).updatePassword(Mockito.any(), Mockito.any(),Mockito.any());
        mockMvc.perform(post("/gymapp/login/update")
                .param("username", "testUser")
                .param("oldPassword", "oldPassword")
                .param("newPassword", "newPassword"))
                .andExpect(status().isOk());

        verify(loginService).updatePassword("testUser", "oldPassword", "newPassword");
    }

    @Test
    public void testUpdatePasswordFailure() throws Exception {
        doThrow(new LoginException("Password update failed")).when(loginService).updatePassword(Mockito.any(), Mockito.any(),Mockito.any());

        mockMvc.perform(post("/gymapp/login/update")
                .param("username", "testUser")
                .param("oldPassword", "oldPassword")
                .param("newPassword", "newPassword"))
                .andExpect(status().isBadRequest());
    }

}

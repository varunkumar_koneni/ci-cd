package com.epam.testing.controller;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.TraineeController;
import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TraineeProfileResponseDTO;
import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TraineeRegistrationRequestDTO;
import com.epam.dto.TraineeTrainingsResponseDTO;
import com.epam.dto.TrainerDTO;
import com.epam.dto.UpdateTraineeProfileDTO;
import com.epam.dto.UpdateTrainerListRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.service.TraineeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TraineeController.class)
public class TraineeControllerTesting {
	
	@MockBean
	TraineeService traineeService;
	
	@Autowired
	MockMvc mockMvc;
	
    @Test
    public void testRegisterTrainee() throws Exception {
        TraineeRegistrationRequestDTO request = new TraineeRegistrationRequestDTO();
        request.setFirstName("varun");
        request.setLastName("kumar");
        request.setEmail("konenivarun123@gmail.com");
        request.setAddress("koneni vandla palli");
        

        RegistrationResponseDTO response = new RegistrationResponseDTO();
        response.setUsername("varun123");
        response.setPassword("12345");

        when(traineeService.registerTrainee(Mockito.any())).thenReturn(response);

        mockMvc.perform(post("/gymapp/trainees/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("varun123"));

        verify(traineeService).registerTrainee(Mockito.any());
    }
    
    @Test
    public void testRegisterTrainee_InvalidRequest() throws Exception {
        TraineeRegistrationRequestDTO request = new TraineeRegistrationRequestDTO();

        mockMvc.perform(post("/gymapp/trainees/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isBadRequest());

        verify(traineeService, never()).registerTrainee(Mockito.any());
    }
    
    
    @Test
    public void testGetTraineeProfile_Success() throws Exception {
        TraineeProfileResponseDTO response = new TraineeProfileResponseDTO();
        response.setFirstName("varun");

        when(traineeService.getTraineeProfile("varun123")).thenReturn(response);

        mockMvc.perform(get("/gymapp/trainees/{username}", "varun123"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("varun"));

        verify(traineeService).getTraineeProfile("varun123");
    }
    
    @Test
    public void testGetTraineeProfile_NotFound() throws Exception {
        when(traineeService.getTraineeProfile("varun")).thenThrow(new TraineeNotFoundException("Trainee not found"));

        mockMvc.perform(get("/gymapp/trainees/{username}", "varun"))
                .andExpect(status().isBadRequest());

        verify(traineeService).getTraineeProfile("varun");
    }
    

    @Test
    public void testUpdateTraineeProfile() throws Exception {
    	
    	UpdateTraineeProfileDTO updateTraineeProfileDTO= new UpdateTraineeProfileDTO();
        TraineeProfileUpdateResponseDTO response = new TraineeProfileUpdateResponseDTO();

        when(traineeService.updateTraineeProfile(Mockito.any())).thenReturn(response);

        mockMvc.perform(put("/gymapp/trainees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updateTraineeProfileDTO)))
                .andExpect(status().isOk());

        verify(traineeService).updateTraineeProfile(Mockito.any());
    }
    
    @Test
    public void testDeleteTraineeProfile() throws Exception {
        doNothing().when(traineeService).deleteTraineeProfile(Mockito.anyString());
        mockMvc.perform(delete("/gymapp/trainees/{username}", "testUser"))
                .andExpect(status().isOk());
        verify(traineeService).deleteTraineeProfile("testUser");
    }
    
    @Test
    public void testGetNotAssignedActiveTrainers() throws Exception {
        List<TrainerDTO> response = new ArrayList<>();

        when(traineeService.getNotAssignedActiveTrainers(Mockito.anyString())).thenReturn(response);

        mockMvc.perform(get("/gymapp/trainees/not-assigned-trainers")
                .param("username", "testUser"))
                .andExpect(status().isOk());

        verify(traineeService).getNotAssignedActiveTrainers("testUser");
    }
    
    @Test
    public void testGetTraineeTrainingsList() throws Exception {
        List<TraineeTrainingsResponseDTO> response = new ArrayList<>();

        when(traineeService.getTraineeTrainingsList(Mockito.anyString())).thenReturn(response);

        mockMvc.perform(get("/gymapp/trainees/trainingsList")
                .param("username", "testUser"))
                .andExpect(status().isOk());

        verify(traineeService).getTraineeTrainingsList("testUser");
    }
    

    @Test
    public void testUpdateTraineeTrainersList() throws Exception {
    	
    	UpdateTrainerListRequestDTO updateTrainerListRequestDTO = new UpdateTrainerListRequestDTO(); 
        List<TrainerDTO> response = new ArrayList<>();

        when(traineeService.updateTraineeTrainersList(Mockito.any())).thenReturn(response);

        mockMvc.perform(put("/gymapp/trainees/update-trainers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updateTrainerListRequestDTO)))
                .andExpect(status().isOk());

        verify(traineeService).updateTraineeTrainersList(Mockito.any());
    }

}

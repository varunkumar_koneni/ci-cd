package com.epam.testing.controller;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.TraineeController;
import com.epam.controller.TrainingController;
import com.epam.dto.AddTrainingRequestDTO;
import com.epam.service.TrainingServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.MediaType;


@WebMvcTest(TrainingController.class)
public class TrainingControllerTesting {
	
	@MockBean
	TrainingServiceImpl trainingService;
	
	@Autowired
	MockMvc mockMvc;
	
	
    @Test
    public void testAddTraining() throws Exception {
        AddTrainingRequestDTO request = new AddTrainingRequestDTO();

        mockMvc.perform(post("/gymapp/trainings/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk());

        verify(trainingService).addTraining(Mockito.any());
    }

}

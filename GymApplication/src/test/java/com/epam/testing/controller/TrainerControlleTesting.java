package com.epam.testing.controller;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.TraineeController;
import com.epam.controller.TrainerController;
import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TraineeRegistrationRequestDTO;
import com.epam.dto.TrainerProfileResponseDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.dto.TrainerRegistrationRequestDTO;
import com.epam.dto.TrainerTrainingsResponseDTO;
import com.epam.dto.UpdateTrainerProfileDTO;
import com.epam.exception.TrainerNotFoundException;
import com.epam.service.TrainerService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TrainerController.class)
public class TrainerControlleTesting {
	
	@MockBean
	TrainerService trainerService;
	
	@Autowired
	MockMvc mockMvc;
	
    @Test
    public void testRegisterTrainer_Success() throws Exception {
        TrainerRegistrationRequestDTO request = new TrainerRegistrationRequestDTO();
        request.setFirstName("varun");
        request.setLastName("kumar");
        request.setEmail("varun@example.com");
        request.setSpecialization("fitness");

        RegistrationResponseDTO response = new RegistrationResponseDTO();
        response.setUsername("varun");

        when(trainerService.registerTrainer(Mockito.any())).thenReturn(response);

        mockMvc.perform(post("/gymapp/trainer/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("varun"));

        verify(trainerService).registerTrainer(Mockito.any());
    }
    
    @Test
    public void testRegisterTrainer_InvalidRequest() throws Exception {
        TrainerRegistrationRequestDTO request = new TrainerRegistrationRequestDTO();

        mockMvc.perform(post("/gymapp/trainer/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isBadRequest());

        verify(trainerService, never()).registerTrainer(Mockito.any());
    }
    
    @Test
    public void testGetTrainerProfile_Success() throws Exception {
        TrainerProfileResponseDTO response = new TrainerProfileResponseDTO();
        response.setFirstName("varun");

        when(trainerService.getTrainerProfile("varun")).thenReturn(response);

        mockMvc.perform(get("/gymapp/trainer/{username}", "varun"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName").value("varun"));

        verify(trainerService).getTrainerProfile("varun");
    }

    @Test
    public void testGetTrainerProfile_NotFound() throws Exception {
        when(trainerService.getTrainerProfile("varun")).thenThrow(new TrainerNotFoundException("Trainer not found"));

        mockMvc.perform(get("/gymapp/trainer/{username}", "varun"))
                .andExpect(status().isBadRequest());

        verify(trainerService).getTrainerProfile("varun");
    }

    @Test
    public void testUpdateTrainerProfile_Success() throws Exception {
        UpdateTrainerProfileDTO request = new UpdateTrainerProfileDTO();
        request.setUsername("varun");

        TrainerProfileUpdateResponseDTO response = new TrainerProfileUpdateResponseDTO();
        response.setUsername("varun");

        when(trainerService.updateTrainerProfile(Mockito.any())).thenReturn(response);

        mockMvc.perform(put("/gymapp/trainer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username").value("varun"));

        verify(trainerService).updateTrainerProfile(Mockito.any());
    }
    @Test
    public void testGetTrainerTrainingsList_Success() throws Exception {
        List<TrainerTrainingsResponseDTO> response = new ArrayList<>();
        // Add test data...

        when(trainerService.getTrainerTrainingsList("johndoe")).thenReturn(response);

        mockMvc.perform(get("/gymapp/trainer/{username}/trainings", "johndoe"))
                .andExpect(status().isOk());

        verify(trainerService).getTrainerTrainingsList("johndoe");
    }
}

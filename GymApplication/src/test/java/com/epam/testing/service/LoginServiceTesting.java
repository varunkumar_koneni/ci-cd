package com.epam.testing.service;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.exception.LoginException;
import com.epam.model.User;
import com.epam.repository.UserRepository;
import com.epam.service.LoginServiceImpl;

@ExtendWith(MockitoExtension.class)
public class LoginServiceTesting {
	
	
	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	LoginServiceImpl loginService;
	
	
	private User user;
    @BeforeEach
    public void setUp() {
    	
    	 user = User.builder()
                 .firstName("varun")
                 .lastName("kumar")
                 .isActive(true)
                 .email("varun@example.com")
                 .username("varun")
                 .password("1234")
                 .build();
    }
	
    @Test
    public void testLoginSuccessful() {
        
        when(userRepository.findByUsername(Mockito.any())).thenReturn(user);

        loginService.loginUser("varun", "1234");
        
        verify(userRepository).findByUsername("varun");
    }
    
    @Test
    public void testLoginFailure() { 	
        when(userRepository.findByUsername(Mockito.any())).thenReturn(null);
        assertThrows(LoginException.class, () -> loginService.loginUser("varun", "varun@123"));
        verify(userRepository).findByUsername("varun");
    }
    
    @Test
    public void testLoginWithWrongPassword() {
   
        when(userRepository.findByUsername(Mockito.any())).thenReturn(user);
        assertThrows(LoginException.class, () -> loginService.loginUser("varun", "varun@123"));  
        verify(userRepository).findByUsername("varun");
    }
    
    @Test
    public void testUpdatePasswordSuccess() throws LoginException {

       when(userRepository.findByUsername(Mockito.any())).thenReturn(user);

       when(userRepository.save(Mockito.any(User.class))).thenReturn(user);

       loginService.updatePassword("varun", "1234", "varun@123");

        assertEquals("varun@123", user.getPassword());
        verify(userRepository).save(user);
    }
    
    @Test
    public void testUpdatePasswordFailure() throws LoginException {

       when(userRepository.findByUsername(Mockito.any())).thenReturn(null);
       assertThrows(LoginException.class, () -> loginService.updatePassword("varun", "1234", "varun@123"));  

    }
    
    @Test
    public void testUpdatePasswordFailureWrongOldPassword() throws LoginException {

       when(userRepository.findByUsername(Mockito.any())).thenReturn(user);
       assertThrows(LoginException.class, () -> loginService.updatePassword("varun", "1234276984ry", "varun@123"));  

    }
    
    
	

}

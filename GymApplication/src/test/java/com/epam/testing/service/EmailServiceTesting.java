package com.epam.testing.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;

import com.epam.dto.TraineeDTO;
import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TrainerDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.model.EmailRequest;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.TrainingType;
import com.epam.model.User;
import com.epam.service.EmailService;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTesting {
	
	@Mock
    private KafkaTemplate<String, EmailRequest> kafkaEmailTemplate;
	
	@Mock
    private EmailRequest emailRequest;


	@InjectMocks
	EmailService emailService;
	
    
    private User user;
    private Trainer trainer;
    private TrainingType trainingType;
    private Trainee trainee;

    @BeforeEach
    public void setUp() {
    	
    	 user = User.builder()
                 .firstName("varun")
                 .lastName("kumar")
                 .isActive(true)
                 .email("varun@example.com")
                 .username("konenivarun123")
                 .password("1234")
                 .build();

        
        trainingType= TrainingType.builder().trainingTypeName("fitness").build();
        
        trainer = Trainer.builder()
                .user(user)
                .specialization(trainingType)
                .traineeList(new ArrayList<Trainee>())
                .build();
        
        trainee = Trainee.builder()
                .user(user)
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .address("tirupathi")
                .trainerList(new ArrayList<Trainer>())
                .build();
        
    }
	
    @Test
    public void testSendRegistrationEmailToTrainee() {
        String username = "varun";
        String password = "1234";
        String recipientEmail = "konenivarun123@gmail.com";

        when(kafkaEmailTemplate.send(Mockito.any(Message.class))).thenReturn(null);

        emailService.sendRegistrationEmailToTrainee(username, password, recipientEmail);

        verify(emailRequest).setTo(recipientEmail);
        verify(emailRequest).setSubject("Registration Successfull");
    }
	
    @Test
    public void testSendRegistrationEmailToTrainer() {
        String username = "varun";
        String password = "1234";
        String recipientEmail = "konenivarun123@gmail.com";

        when(kafkaEmailTemplate.send(Mockito.any(Message.class))).thenReturn(null);

        emailService.sendRegistrationEmailToTrainer(username, password, recipientEmail);

        verify(emailRequest).setTo(recipientEmail);
        verify(emailRequest).setSubject("Registration Successfull");

    }
    
    @Test
    public void testSendProfileUpdatedEmailToTrainee() {
        String emailId = "konenivarun123@gmail.com";
        TraineeProfileUpdateResponseDTO traineeProfileUpdateResponseDTO =TraineeProfileUpdateResponseDTO.builder()
                .username("varun123")
                .firstName("varun")
                .lastName("kumar")
                .isActive(true)
                .address("tirupathi")
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .trainers(new ArrayList<TrainerDTO>())
                .build();
        
        when(kafkaEmailTemplate.send(Mockito.any(Message.class))).thenReturn(null);


        emailService.sendProfileUpdatedEmailToTrainee(emailId, traineeProfileUpdateResponseDTO);

        verify(emailRequest).setTo(emailId);
        verify(emailRequest).setSubject("Profile Updation");
        verify(kafkaEmailTemplate).send(Mockito.any(Message.class));
    }
    
    @Test
    public void testSendProfileUpdatedEmailToTrainer() {
        String emailId = "konenivarun123@gmail.com";
        TrainerProfileUpdateResponseDTO trainerProfileUpdateResponseDTO = TrainerProfileUpdateResponseDTO.builder()
                .username("varun123")
                .firstName("varun")
                .lastName("kumar")
                .specialization(null)
                .isActive(true)
                .trainees(new ArrayList<TraineeDTO>())
                .build();

        when(kafkaEmailTemplate.send(Mockito.any(Message.class))).thenReturn(null);

        emailService.sendProfileUpdatedEmailToTrainer(emailId, trainerProfileUpdateResponseDTO);

        verify(emailRequest).setTo(emailId);
        verify(emailRequest).setSubject("Trainer Profile Updated");
        verify(kafkaEmailTemplate).send(Mockito.any(Message.class));

    }
    @Test
    public void testSendTrainingConfirmationEmailToTrainer() {
        String emailId = "konenivarun123@gmail.com";
    	Training training = Training.builder()
                .trainee(trainee)
                .trainingName("6 pack")
                .trainingDate(LocalDate.of(2023, 8, 13))
                .trainingType(trainingType)
                .trainingDuration(60)
                .trainer(trainer) 
                .build();

        when(kafkaEmailTemplate.send(Mockito.any(Message.class))).thenReturn(null);
        emailService.sendTrainingConfirmationEmailToTrainer(emailId, training);

        verify(emailRequest).setTo(emailId);
        verify(emailRequest).setSubject("Training Confirmation - Trainer");
        verify(kafkaEmailTemplate).send(Mockito.any(Message.class));
    }

    @Test
    public void testSendTrainingConfirmationEmailToTrainee() {
        String emailId = "konenivarun123@gmail.com";
    	Training training = Training.builder()
                .trainee(trainee)
                .trainingName("6 pack")
                .trainingDate(LocalDate.of(2023, 8, 13))
                .trainingType(trainingType)
                .trainingDuration(60)
                .trainer(trainer) 
                .build();

        when(kafkaEmailTemplate.send(Mockito.any(Message.class))).thenReturn(null);
        emailService.sendTrainingConfirmationEmailToTrainee(emailId, training);

        verify(emailRequest).setTo(emailId);
        verify(emailRequest).setSubject("Training Confirmation - Trainee");
        verify(kafkaEmailTemplate).send(Mockito.any(Message.class));
    }
}

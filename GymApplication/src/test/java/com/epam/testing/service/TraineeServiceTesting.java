package com.epam.testing.service;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.converter.TraineeConverter;
import com.epam.converter.TrainerConverter;
import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TraineeProfileResponseDTO;
import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TraineeRegistrationRequestDTO;
import com.epam.dto.TraineeTrainingsResponseDTO;
import com.epam.dto.TrainerDTO;
import com.epam.dto.UpdateTraineeProfileDTO;
import com.epam.dto.UpdateTrainerListRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.User;
import com.epam.repository.TraineeRepository;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.UserRepository;
import com.epam.service.EmailService;
import com.epam.service.TraineeServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TraineeServiceTesting {
	

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private EmailService emailService;

    @Mock
    private TraineeConverter traineeConverter;
    
    @Mock
    private TrainerConverter trainerConverter;

    @Mock
    private PasswordEncoder passwordEncoder;
    
    @Mock
    UserRepository userRepository;

    @InjectMocks
    private TraineeServiceImpl traineeService;
    
    @Test
    public void testRegisterTrainee() {
        TraineeRegistrationRequestDTO request = new TraineeRegistrationRequestDTO();
        request.setFirstName("varun");
        request.setLastName("kumar");
        request.setEmail("konenivarun123@gmail.com");
        request.setDateOfBirth(LocalDate.of(1990, 1, 1));
        request.setAddress("Guntur");
        
        String username = request.getEmail().split("@")[0];
        String password = "12345";

        User user = User.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .isActive(true)
                .email(request.getEmail())
                .username(username)
                .password("encodedPassword")  
                .build();

        Trainee trainee = Trainee.builder()
                .user(user)
                .dateOfBirth(request.getDateOfBirth())
                .address(request.getAddress())
                .build();
        
        when(passwordEncoder.encode(Mockito.any())).thenReturn("encodedPassword");
        when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        when(traineeRepository.save(Mockito.any(Trainee.class))).thenReturn(trainee);

        RegistrationResponseDTO response = RegistrationResponseDTO.builder()
                .username(username)
                .password(password)
                .build();
        when(traineeConverter.toProfileResponseDTO(username, password)).thenReturn(response);

        Mockito.doNothing().when(emailService).sendRegistrationEmailToTrainee(Mockito.any(), Mockito.any(), Mockito.any());

        RegistrationResponseDTO result = traineeService.registerTrainee(request);

        assertEquals(username, result.getUsername());
        assertEquals(password, result.getPassword());
    }
    
    
    @Test
    public void testGetTraineeProfile_Positive() throws TraineeNotFoundException {
        String username = "varun";
        Trainee trainee = new Trainee();
        
        TraineeProfileResponseDTO traineeProfileResponseDTO = TraineeProfileResponseDTO.builder()
                .firstName("varun")
                .lastName("kumar")
                .isActive(true)
                .address("tirupathi")
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .trainers(new ArrayList<TrainerDTO>())
                .build();
        
        when(traineeRepository.findByUserUsername(username)).thenReturn(trainee);
        when(traineeConverter.toProfileResponseDTO(Mockito.any(Trainee.class))).thenReturn(traineeProfileResponseDTO);

        TraineeProfileResponseDTO response = traineeService.getTraineeProfile(username);

        assertNotNull(response);
        
        assertEquals(traineeProfileResponseDTO.getFirstName(), response.getFirstName());
        assertEquals(traineeProfileResponseDTO.getLastName(), response.getLastName());
        assertEquals(traineeProfileResponseDTO.isActive(), response.isActive());
        assertEquals(traineeProfileResponseDTO.getAddress(), response.getAddress());
        assertEquals(traineeProfileResponseDTO.getDateOfBirth(), response.getDateOfBirth());
    }
    
    @Test
    public void testGetTraineeProfile_Negative() {
        String username = "hari";
        when(traineeRepository.findByUserUsername(username)).thenReturn(null);
        assertThrows(TraineeNotFoundException.class, () -> traineeService.getTraineeProfile(username));
    }
    
    
    @Test
    public void testDeleteTraineeProfile_Positive() throws TraineeNotFoundException {
        String username = "varun";
        Trainee trainee = new Trainee();
        trainee.setUser(new User());
        when(traineeRepository.findByUserUsername(username)).thenReturn(trainee);
        Mockito.doNothing().when(traineeRepository).delete(Mockito.any());
        Mockito.doNothing().when(userRepository).delete(Mockito.any());


        assertDoesNotThrow(() -> traineeService.deleteTraineeProfile(username));

        verify(traineeRepository).delete(Mockito.any());
        verify(userRepository).delete(Mockito.any());
    }

    @Test
    public void testDeleteTraineeProfile_Negative() {
        String username = "varun";
        when(traineeRepository.findByUserUsername(username)).thenReturn(null);

        assertThrows(TraineeNotFoundException.class, () -> traineeService.deleteTraineeProfile(username));

    }
    
    @Test
    public void testUpdateTraineeProfile_Positive() throws TraineeNotFoundException {
        UpdateTraineeProfileDTO updateDTO = new UpdateTraineeProfileDTO();
        updateDTO.setUsername("varun@123");
        updateDTO.setFirstName("varun");
        updateDTO.setLastName("kumar");
        updateDTO.setActive(true);
        updateDTO.setAddress("tirupathi");
        updateDTO.setDateOfBirth(LocalDate.of(1995, 3, 15));

        User user = User.builder()
                .firstName(updateDTO.getFirstName())
                .lastName(updateDTO.getLastName())
                .isActive(true)
                .email("konenivarun123@gmail.com")
                .username("varun@123")
                .password("12345")  
                .build();

        Trainee trainee = Trainee.builder()
                .user(user)
                .dateOfBirth(updateDTO.getDateOfBirth())
                .address(updateDTO.getAddress())
                .build();
        TraineeProfileUpdateResponseDTO traineeProfileUpdateResponseDTO=new TraineeProfileUpdateResponseDTO();

        when(traineeRepository.findByUserUsername(Mockito.any())).thenReturn(trainee);
        when(traineeRepository.save(Mockito.any(Trainee.class))).thenReturn(trainee); 
        when(traineeConverter.toProfileUpdateResponseDTO(Mockito.any(), Mockito.any())).thenReturn(traineeProfileUpdateResponseDTO);
        Mockito.doNothing().when(emailService).sendProfileUpdatedEmailToTrainee(Mockito.anyString(), Mockito.any());


        assertDoesNotThrow(() -> traineeService.updateTraineeProfile(updateDTO));

        verify(traineeRepository).save(trainee);
    }

    @Test
    public void testUpdateTraineeProfile_Negative() {
        UpdateTraineeProfileDTO updateDTO = new UpdateTraineeProfileDTO();
        updateDTO.setUsername("nonExistentUsername");

        when(traineeRepository.findByUserUsername(updateDTO.getUsername())).thenReturn(null);

        assertThrows(TraineeNotFoundException.class, () -> traineeService.updateTraineeProfile(updateDTO));

    }
    @Test
    public void testGetNotAssignedActiveTrainers_Positive() throws TraineeNotFoundException {
        String traineeUsername = "Varun";

        User user = User.builder()
                .firstName("varun")
                .lastName("kumar")
                .isActive(true)
                .email("konenivarun123@gmail.com")
                .username("varun@123")
                .password("12345")  
                .build();
        List<Trainer> trainerss = new ArrayList<>();

        Trainee trainee = Trainee.builder()
                .user(user)
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .address("tirupathi")
                .trainerList(trainerss)
                .build(); 
        
        
        List<Trainee> trainees = new ArrayList<>();

        Trainer trainer = Trainer.builder()
                .user(user)
                .specialization(null)
                .traineeList(trainees)
                .build();
        
        List<Trainer> assignedTrainers = new ArrayList<>();
        assignedTrainers.add(trainer);
        
        when(traineeRepository.findByUserUsername(Mockito.any())).thenReturn(trainee);


        List<Trainer> trainers = new ArrayList<>();

        trainees.add(trainee);

        trainers.add(trainer);
        when(trainerRepository.findAll()).thenReturn(trainers);

        TrainerDTO trainerDTO = TrainerDTO.builder()
                .username(trainer.getUser().getUsername())
                .firstName(trainer.getUser().getFirstName())
                .lastName(trainer.getUser().getLastName())
                .specialization(null)
                .build();

        when(traineeConverter.mapToTrainerDTO(Mockito.any())).thenReturn(trainerDTO);

        List<TrainerDTO> result = traineeService.getNotAssignedActiveTrainers(traineeUsername);

        assertEquals(trainerDTO.getFirstName(), result.get(0).getFirstName()); 
    }
    
    @Test
    public void testGetTraineeTrainingsList() throws TraineeNotFoundException {
        String username = "varun";
        User user = User.builder()
                .firstName("varun")
                .lastName("kumar")
                .isActive(true)
                .email("konenivarun123@gmail.com")
                .username("varun")
                .password("12345")  
                .build();
        Trainee trainee = Trainee.builder()
                .id(1)
                .user(user)
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .address("tirupathi")
                .build();
        List<Trainee> trainees = new ArrayList<>();

        Trainer trainer = Trainer.builder()
                .user(user)
                .specialization(null)
                .traineeList(trainees)
                .build();
        
        Training training = Training.builder()
                .id(1)
                .trainee(trainee)
                .trainingName("6 pack")
                .trainingDate(LocalDate.of(2023, 8, 13))
                .trainingType(null)
                .trainingDuration(60)
                .trainer(trainer) 
                .build();
        
        when(traineeRepository.findByUserUsername(username)).thenReturn(trainee);

        List<Training> traineeTrainings = new ArrayList<>();
        traineeTrainings.add(training);

        when(trainingRepository.findByTraineeId(trainee.getId())).thenReturn(traineeTrainings);
        TraineeTrainingsResponseDTO responseDTO = TraineeTrainingsResponseDTO.builder()
                .trainingName("6 pack")
                .trainingDate(LocalDate.of(1990, 1, 1))
                .trainingType("fitness")
                .trainingDuration(60)
                .trainerName("hari")
                .build();
        

        when(traineeConverter.convertToResponseDTO(training)).thenReturn(responseDTO);

        List<TraineeTrainingsResponseDTO> result = traineeService.getTraineeTrainingsList(username);

        assertNotNull(result);
        assertEquals(1, result.size());
        
        TraineeTrainingsResponseDTO expectedResponse = TraineeTrainingsResponseDTO.builder()
                .trainingName("6 pack")
                .trainingDate(LocalDate.of(2023, 8, 13))
                .trainingType("fitness")
                .trainingDuration(60)
                .trainerName("hari")
                .build();

        assertEquals(expectedResponse.getTrainerName(), result.get(0).getTrainerName());

        verify(traineeRepository, times(1)).findByUserUsername(username);
        verify(trainingRepository, times(1)).findByTraineeId(trainee.getId());
        verify(traineeConverter, times(1)).convertToResponseDTO(training);
    }
    
    @Test
    public void testUpdateTraineeTrainersList() throws TraineeNotFoundException, TrainerNotFoundException {
        String traineeUsername = "varun";

        User user = User.builder()
                .firstName("varun")
                .lastName("kumar")
                .isActive(true)
                .email("konenivarun123@gmail.com")
                .username(traineeUsername)
                .password("12345")
                .build();
        List<Trainer> trainerss = new ArrayList<>();

        Trainee trainee = Trainee.builder()
                .id(1)
                .user(user)
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .address("tirupathi")
                .trainerList(trainerss)
                .build();

        List<Trainee> trainees = new ArrayList<>();
        trainees.add(trainee);

        Trainer trainer = Trainer.builder()
        		.id(1)
                .user(user)
                .specialization(null)
                .traineeList(trainees)
                .build();



        when(traineeRepository.findByUserUsername(traineeUsername)).thenReturn(trainee);

        List<String> trainers = new ArrayList<>();
        trainers.add("kumar");

        UpdateTrainerListRequestDTO request = new UpdateTrainerListRequestDTO();
        request.setTraineeUsername(traineeUsername);
        request.setTrainersList(trainers);

        when(trainerRepository.findByUserUsername(Mockito.anyString())).thenReturn(trainer);
        when(traineeRepository.save(Mockito.any(Trainee.class))).thenReturn(trainee);
        TrainerDTO response=TrainerDTO.builder().username("varun")
        		.firstName("kumar").lastName("kumar")
        		.specialization(null).build();
        
        when(traineeConverter.mapToTrainerDTO(trainer)).thenReturn(response);
        
        List<TrainerDTO> result = traineeService.updateTraineeTrainersList(request);

        
        assertEquals(response.getFirstName(), result.get(0).getFirstName());
        
       

    }
    
    @Test
    public void testUpdateTraineeTrainersListFails() throws TraineeNotFoundException, TrainerNotFoundException {
        String traineeUsername = "varun";

        User user = User.builder()
                .firstName("varun")
                .lastName("kumar")
                .isActive(true)
                .email("konenivarun123@gmail.com")
                .username(traineeUsername)
                .password("12345")
                .build();
        List<Trainer> trainerss = new ArrayList<>();

        Trainee trainee = Trainee.builder()
                .id(1)
                .user(user)
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .address("tirupathi")
                .trainerList(trainerss)
                .build();

        List<Trainee> trainees = new ArrayList<>();
        trainees.add(trainee);


        when(traineeRepository.findByUserUsername(traineeUsername)).thenReturn(trainee);

        List<String> trainers = new ArrayList<>();
        trainers.add("kumar");

        UpdateTrainerListRequestDTO request = new UpdateTrainerListRequestDTO();
        request.setTraineeUsername(traineeUsername);
        request.setTrainersList(trainers);

        when(trainerRepository.findByUserUsername(Mockito.anyString())).thenReturn(null);

        assertThrows(TrainerNotFoundException.class, () -> traineeService.updateTraineeTrainersList(request));

 

    }
   

    


}

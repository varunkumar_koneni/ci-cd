package com.epam.testing.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;

import com.epam.dto.AddTrainingRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.EmailRequest;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.TrainingType;
import com.epam.model.User;
import com.epam.repository.TraineeRepository;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.TrainingTypeRepository;
import com.epam.service.EmailService;
import com.epam.service.TrainingServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TrainingServiceTesting {
	
	   @InjectMocks
	    private TrainingServiceImpl trainingService;

	    @Mock
	    private TraineeRepository traineeRepository;

	    @Mock
	    private TrainerRepository trainerRepository;

	    @Mock
	    private TrainingRepository trainingRepository;

	    @Mock
	    private TrainingTypeRepository trainingTypeRepository;

	    @Mock
	    private EmailService emailService;

	    @Mock
	    private KafkaTemplate<String, EmailRequest> kafkaReportTemplate;
	    
	    
	    private User user;
	    private Trainer trainer;
	    private TrainingType trainingType;
	    private Trainee trainee;

	    @BeforeEach
	    public void setUp() {
	    	
	    	 user = User.builder()
	                 .firstName("varun")
	                 .lastName("kumar")
	                 .isActive(true)
	                 .email("varun@example.com")
	                 .username("konenivarun123")
	                 .password("1234")
	                 .build();

	        
	        trainingType= TrainingType.builder().trainingTypeName("fitness").build();
	        
	        trainer = Trainer.builder()
	                .user(user)
	                .specialization(trainingType)
	                .traineeList(new ArrayList<Trainee>())
	                .build();
	        
	        trainee = Trainee.builder()
	                .user(user)
	                .dateOfBirth(LocalDate.of(1990, 1, 1))
	                .address("tirupathi")
	                .trainerList(new ArrayList<Trainer>())
	                .build();
	        
	    }

	    @Test
	    public void testAddTraining() throws TrainerNotFoundException, TraineeNotFoundException {
	        AddTrainingRequestDTO request = new AddTrainingRequestDTO();
	        request.setTraineeUsername("varun");
	        request.setTrainerUsername("hari");
	        request.setTrainingName("6 pack");
	        request.setTrainingDate(LocalDate.now());
	        request.setTrainingDuration(5);

	    	Training training = Training.builder()
	                .trainee(trainee)
	                .trainingName("6 pack")
	                .trainingDate(LocalDate.of(2023, 8, 13))
	                .trainingType(trainingType)
	                .trainingDuration(60)
	                .trainer(trainer) 
	                .build();
	    	
	        when(traineeRepository.findByUserUsername(Mockito.any())).thenReturn(trainee);
	        when(trainerRepository.findByUserUsername(Mockito.any())).thenReturn(trainer);
	        when(traineeRepository.save(Mockito.any(Trainee.class))).thenReturn(trainee); 
	        when(trainingRepository.save(Mockito.any(Training.class))).thenReturn(training); 
	        
	        Mockito.doNothing().when(emailService).sendTrainingConfirmationEmailToTrainee(Mockito.any(), Mockito.any());
	        Mockito.doNothing().when(emailService).sendTrainingConfirmationEmailToTrainer(Mockito.any(), Mockito.any());
	        when(kafkaReportTemplate.send(Mockito.any(Message.class))).thenReturn(null);

	        trainingService.addTraining(request);

	    }
	    
	    @Test
	    public void testAddTrainingFailsNoTrainee() {
	        AddTrainingRequestDTO request = new AddTrainingRequestDTO();
	        request.setTraineeUsername("varun");
	        request.setTrainerUsername("hari");
	        request.setTrainingName("6 pack");
	        request.setTrainingDate(LocalDate.now());
	        request.setTrainingDuration(5);
	        when(traineeRepository.findByUserUsername(Mockito.any())).thenReturn(null);
	        assertThrows(TraineeNotFoundException.class, () -> trainingService.addTraining(request));
	    }
	    
	    @Test
	    public void testAddTrainingFailsNoTrainer() {
	        AddTrainingRequestDTO request = new AddTrainingRequestDTO();
	        request.setTraineeUsername("varun");
	        request.setTrainerUsername("hari");
	        request.setTrainingName("6 pack");
	        request.setTrainingDate(LocalDate.now());
	        request.setTrainingDuration(5);
	        when(traineeRepository.findByUserUsername(Mockito.any())).thenReturn(trainee);

	        when(trainerRepository.findByUserUsername(Mockito.any())).thenReturn(null);
	        assertThrows(TrainerNotFoundException.class, () -> trainingService.addTraining(request));
	    }

}

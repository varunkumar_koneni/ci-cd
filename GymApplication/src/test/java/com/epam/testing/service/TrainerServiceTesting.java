package com.epam.testing.service;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.converter.TrainerConverter;
import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TrainerProfileResponseDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.dto.TrainerRegistrationRequestDTO;
import com.epam.dto.TrainerTrainingsResponseDTO;
import com.epam.dto.UpdateTrainerProfileDTO;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.TrainingType;
import com.epam.model.User;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.TrainingTypeRepository;
import com.epam.repository.UserRepository;
import com.epam.service.EmailService;
import com.epam.service.TrainerServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TrainerServiceTesting {
	
    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TrainerConverter trainerConverter;

    @Mock
    private EmailService emailService;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @InjectMocks
    private TrainerServiceImpl trainerService;
    
    private User user;
    private Trainer trainer;
    private TrainingType trainingType;
    private Trainee trainee;

    @BeforeEach
    public void setUp() {
    	
    	 user = User.builder()
                 .firstName("varun")
                 .lastName("kumar")
                 .isActive(true)
                 .email("varun@example.com")
                 .username("konenivarun123")
                 .password("1234")
                 .build();

        
        trainingType= TrainingType.builder().trainingTypeName("fitness").build();
        
        trainer = Trainer.builder()
                .user(user)
                .specialization(trainingType)
                .build();
        trainee = Trainee.builder()
                .id(1)
                .user(user)
                .dateOfBirth(LocalDate.of(1990, 1, 1))
                .address("tirupathi")
                .build();
        
    }
    
    
    @Test
    public void testRegisterTrainer_Positive() {
        TrainerRegistrationRequestDTO request = new TrainerRegistrationRequestDTO();
        request.setFirstName("varun");
        request.setLastName("kumar");
        request.setEmail("konenivarun123@gmail.com");
        request.setSpecialization("Fitness");

       

        when(userRepository.save(Mockito.any(User.class))).thenReturn(user);
        when(trainingTypeRepository.findByTrainingTypeName(Mockito.any())).thenReturn(trainingType);
        when(trainerRepository.save(Mockito.any(Trainer.class))).thenReturn(new Trainer());

        RegistrationResponseDTO response = trainerService.registerTrainer(request);

        assertEquals(user.getUsername(), response.getUsername());
        assertNotNull(response);

    }
    
    @Test
    public void testGetTrainerProfile_Positive() throws TrainerNotFoundException {
    	
    	TrainerProfileResponseDTO trainerProfileResponseDTO= TrainerProfileResponseDTO.builder()
        .firstName(trainer.getUser().getFirstName())
        .lastName(trainer.getUser().getLastName())
        .isActive(trainer.getUser().isActive())
        .specialization(trainer.getSpecialization().getTrainingTypeName())
        .build();
    	
        when(trainerRepository.findByUserUsername(Mockito.any())).thenReturn(trainer);
        when(trainerConverter.toProfileResponseDTO(Mockito.any(Trainer.class))).thenReturn(trainerProfileResponseDTO);

        TrainerProfileResponseDTO response = trainerService.getTrainerProfile("konenivarun123");

        assertEquals(trainerProfileResponseDTO.getFirstName(), response.getFirstName());
        assertNotNull(response);
    }
    
    @Test
    public void testGetTrainerProfile_Negative() {
        String username = "hari";
        when(trainerRepository.findByUserUsername(username)).thenReturn(null);
        assertThrows(TrainerNotFoundException.class, () -> trainerService.getTrainerProfile(username));
    }
    
    @Test
    public void testUpdateTrainerProfile_Positive() throws TrainerNotFoundException {
    	
        UpdateTrainerProfileDTO updateRequest = new UpdateTrainerProfileDTO();
        updateRequest.setUsername("varun123");
        updateRequest.setFirstName("varun");
        updateRequest.setLastName("kumar");
        updateRequest.setSpecialization(trainingType);
        updateRequest.setActive(true);

        when(trainerRepository.findByUserUsername(Mockito.any())).thenReturn(trainer);
        when(trainerRepository.save(Mockito.any(Trainer.class))).thenReturn(trainer); 
        TrainerProfileUpdateResponseDTO trainerProfileUpdateResponseDTO=new TrainerProfileUpdateResponseDTO();

        when(trainerConverter.toProfileUpdateResponseDTO(Mockito.any(), Mockito.any())).thenReturn(trainerProfileUpdateResponseDTO);


        Mockito.doNothing().when(emailService).sendProfileUpdatedEmailToTrainer(Mockito.anyString(), Mockito.any());


        assertDoesNotThrow(() -> trainerService.updateTrainerProfile(updateRequest));

        verify(trainerRepository).save(trainer);
    }
    
    
    @Test
    public void testGetTrainerTrainingsList_Positive() throws TrainerNotFoundException {
    	Training training = Training.builder()
                .id(1)
                .trainee(trainee)
                .trainingName("6 pack")
                .trainingDate(LocalDate.of(2023, 8, 13))
                .trainingType(trainingType)
                .trainingDuration(60)
                .trainer(trainer) 
                .build();
        

        List<Training> traineeTrainings = new ArrayList<>();
        traineeTrainings.add(training);
        
        TrainerTrainingsResponseDTO response = TrainerTrainingsResponseDTO.builder()
                .trainingName("6 pack")
                .trainingDate(LocalDate.of(2023, 8, 13))
                .trainingType("fitness")
                .trainingDuration(60)
                .traineeName("hari")
                .build();

        when(trainerRepository.findByUserUsername(Mockito.any())).thenReturn(trainer);
        when(trainingRepository.findByTrainerId(Mockito.anyInt())).thenReturn(traineeTrainings);
        when(trainerConverter.convertToResponseDTO(Mockito.any(Training.class))).thenReturn(response);

        List<TrainerTrainingsResponseDTO> result = trainerService.getTrainerTrainingsList("johndoe");

        assertEquals(response.getTraineeName(), result.get(0).getTraineeName());

    }

}

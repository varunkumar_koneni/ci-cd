package com.epam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.exception.LoginException;
import com.epam.model.User;
import com.epam.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void loginUser(String username, String password) throws LoginException {
        log.info("Logging in user: {}", username);
        User user = userRepository.findByUsername(username);
        if (user == null) {
            log.error("User not found: {}", username);
            throw new LoginException("User not found");
        }
        if (!user.getPassword().equals(password)) {
            log.error("Invalid password for user: {}", username);
            throw new LoginException("Invalid password");
        }
        log.info("User logged in successfully: {}", username);
    }

    @Override
    public void updatePassword(String username, String oldPassword, String newPassword) throws LoginException {
        log.info("Changing login password for user: {}", username);
        User user = userRepository.findByUsername(username);
        if (user == null) {
            log.error("User not found: {}", username);
            throw new LoginException("User not found");
        }
        if (!user.getPassword().equals(oldPassword)) {
            log.error("Invalid old password for user: {}", username);
            throw new LoginException("Invalid password");
        }
        user.setPassword(newPassword);
        userRepository.save(user);
        log.info("Login password changed successfully for user: {}", username);
    }
}

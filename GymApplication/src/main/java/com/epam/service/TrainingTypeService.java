package com.epam.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.TrainingTypeDTO;
import com.epam.repository.TrainingTypeRepository;

@Service
public class TrainingTypeService {

	@Autowired
    private  TrainingTypeRepository trainingTypeRepository;
	
	@Autowired
	ModelMapper modelMapper;


    public List<TrainingTypeDTO> getAllTrainingTypes() {
        return trainingTypeRepository.findAll().stream()
                .map(trainingType -> modelMapper.map(trainingType, TrainingTypeDTO.class))
                .toList();
    }
}


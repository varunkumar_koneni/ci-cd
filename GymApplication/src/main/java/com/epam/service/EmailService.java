package com.epam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.model.EmailRequest;
import com.epam.model.Training;
import com.epam.proxy.EmailProxy;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailService {

    @Autowired
    private EmailRequest emailRequest;


    @Autowired
    private KafkaTemplate<String, EmailRequest> kafkaEmailTemplate;

    
    private static final String NOTIFICATION_TOPIC = "notification3";

    public void sendRegistrationEmailToTrainee(String username, String password, String recipientEmail) {
        emailRequest.setTo(recipientEmail);
        emailRequest.setSubject("Registration Successfull");
        emailRequest.setBody("Dear Trainee,\n\nThank you for the registration. Below are your login details:\n\nUsername: " + username + "\nPassword: " + password);

        Message<EmailRequest> email = MessageBuilder.withPayload(emailRequest)
                .setHeader(KafkaHeaders.TOPIC, NOTIFICATION_TOPIC).build();

        kafkaEmailTemplate.send(email);
        log.info("Email notification sent to Kafka for {}", recipientEmail);
    }

    public void sendProfileUpdatedEmailToTrainee(String emailId, TraineeProfileUpdateResponseDTO responseDTO) {
        emailRequest.setTo(emailId);
        emailRequest.setSubject("Profile Updation");

        emailRequest.setBody("Dear"+responseDTO.getFirstName()+" Your profile has been updated successfully.\n\n"
                + "Username: " + responseDTO.getUsername() + "\n"
                + "First Name: " + responseDTO.getFirstName() + "\n"
                + "Last Name: " + responseDTO.getLastName() + "\n"
                + "Active: " + responseDTO.isActive() + "\n"
                + "Address: " + responseDTO.getAddress() + "\n"
                + "Date of Birth: " + responseDTO.getDateOfBirth());

        Message<EmailRequest> email = MessageBuilder.withPayload(emailRequest)
                .setHeader(KafkaHeaders.TOPIC, NOTIFICATION_TOPIC).build();

        kafkaEmailTemplate.send(email);
        log.info("Email notification sent to Kafka for {}", emailId);
    }

    public void sendRegistrationEmailToTrainer(String username, String password, String recipientEmail) {
        emailRequest.setTo(recipientEmail);
        emailRequest.setSubject("Registration Successfull");
        emailRequest.setBody("Dear Trainar,\n\nThank you for the registration. Below are your login details:\n\nUsername: " + username + "\nPassword: " + password);

        Message<EmailRequest> email = MessageBuilder.withPayload(emailRequest)
                .setHeader(KafkaHeaders.TOPIC, NOTIFICATION_TOPIC).build();

        kafkaEmailTemplate.send(email);
        log.info("Email notification sent to Kafka for {}", recipientEmail);
    }

    public void sendProfileUpdatedEmailToTrainer(String emailId, TrainerProfileUpdateResponseDTO responseDTO) {
        emailRequest.setTo(emailId);
        emailRequest.setSubject("Trainer Profile Updated");

        emailRequest.setBody("Your profile has been updated successfully.\n\n"
                + "Username: " + responseDTO.getUsername() + "\n"
                + "First Name: " + responseDTO.getFirstName() + "\n"
                + "Last Name: " + responseDTO.getLastName() + "\n"
                + "Active: " + responseDTO.isActive() + "\n"
                + "Specialization: " + responseDTO.getSpecialization());

        Message<EmailRequest> email = MessageBuilder.withPayload(emailRequest)
                .setHeader(KafkaHeaders.TOPIC, NOTIFICATION_TOPIC).build();

        kafkaEmailTemplate.send(email);
        log.info("Email notification sent to Kafka for {}", emailId);
    }

    public void sendTrainingConfirmationEmailToTrainer(String emailId, Training training) {
        emailRequest.setTo(emailId);
        emailRequest.setSubject("Training Confirmation - Trainer");

        String body = "Dear Trainer,\n\n"
                + "A new training has been added with the following details:\n\n"
                + "Trainee Name: " + training.getTrainee().getUser().getUsername() + "\n"
                + "Training Name: " + training.getTrainingName() + "\n"
                + "Training Date: " + training.getTrainingDate() + "\n"
                + "Training Type: " + training.getTrainingType().getTrainingTypeName() + "\n"
                + "Training Duration: " + training.getTrainingDuration();

      
        emailRequest.setBody(body);

        Message<EmailRequest> email = MessageBuilder.withPayload(emailRequest)
                .setHeader(KafkaHeaders.TOPIC, NOTIFICATION_TOPIC).build();

        kafkaEmailTemplate.send(email);
        log.info("Email notification sent to Kafka for {}", emailId);
    }

    public void sendTrainingConfirmationEmailToTrainee(String emailId, Training training) {
        emailRequest.setTo(emailId);
        emailRequest.setSubject("Training Confirmation - Trainee");

        String body = "Dear Trainee,\n\n"
                + "A new training has been added with the following details:\n\n"
                + "Trainer Name: " + training.getTrainer().getUser().getUsername() + "\n"
                + "Training Name: " + training.getTrainingName() + "\n"
                + "Training Date: " + training.getTrainingDate() + "\n"
                + "Training Type: " + training.getTrainingType().getTrainingTypeName() + "\n"
                + "Training Duration: " + training.getTrainingDuration();

        emailRequest.setBody(body);

        Message<EmailRequest> email = MessageBuilder.withPayload(emailRequest)
                .setHeader(KafkaHeaders.TOPIC, NOTIFICATION_TOPIC).build();

        kafkaEmailTemplate.send(email);
        log.info("Email notification sent to Kafka for {}", emailId);
    }
}

package com.epam.service;

import com.epam.dto.AddTrainingRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.exception.TrainerNotFoundException;

public interface TrainingService {
	
	public void addTraining(AddTrainingRequestDTO request) throws TrainerNotFoundException,TraineeNotFoundException;

}

package com.epam.dto;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Component
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationResponseDTO {
	
    private String username;
    private String password;    

}

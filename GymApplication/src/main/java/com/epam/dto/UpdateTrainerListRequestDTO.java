package com.epam.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateTrainerListRequestDTO {
	
	private String traineeUsername;
    private List<String> trainersList;
}

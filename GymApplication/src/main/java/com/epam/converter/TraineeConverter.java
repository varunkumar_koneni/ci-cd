package com.epam.converter;

import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TraineeProfileResponseDTO;
import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TraineeTrainingsResponseDTO;
import com.epam.dto.TrainerDTO;
import com.epam.dto.UpdateTraineeProfileDTO;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;

@Component
public class TraineeConverter {

    public TraineeProfileResponseDTO toProfileResponseDTO(Trainee trainee) {
        List<TrainerDTO> trainers = trainee.getTrainerList().stream()
                .map(this::mapToTrainerDTO)
                .toList();
        
        return TraineeProfileResponseDTO.builder()
                .firstName(trainee.getUser().getFirstName())
                .lastName(trainee.getUser().getLastName())
                .isActive(trainee.getUser().isActive())
                .address(trainee.getAddress())
                .dateOfBirth(trainee.getDateOfBirth())
                .trainers(trainers)
                .build();
    }

    public TraineeProfileUpdateResponseDTO toProfileUpdateResponseDTO(Trainee trainee, UpdateTraineeProfileDTO updateDto) {
        List<TrainerDTO> trainers = trainee.getTrainerList().stream()
                .map(this::mapToTrainerDTO)
                .toList();
        
        return TraineeProfileUpdateResponseDTO.builder()
                .username(updateDto.getUsername())
                .firstName(updateDto.getFirstName())
                .lastName(updateDto.getLastName())
                .isActive(updateDto.isActive())
                .address(updateDto.getAddress())
                .dateOfBirth(updateDto.getDateOfBirth())
                .trainers(trainers)
                .build();
    }

	public TrainerDTO mapToTrainerDTO(Trainer trainer) {
		return TrainerDTO.builder().username(trainer.getUser().getUsername())
				.firstName(trainer.getUser().getFirstName()).lastName(trainer.getUser().getLastName())
				.specialization(trainer.getSpecialization().getTrainingTypeName()).build();
	}

	public TraineeTrainingsResponseDTO convertToResponseDTO(Training training) {
		return TraineeTrainingsResponseDTO.builder().trainingName(training.getTrainingName())
				.trainingDate(training.getTrainingDate()).trainingType(training.getTrainingType().getTrainingTypeName())
				.trainingDuration(training.getTrainingDuration())
				.trainerName(training.getTrainer().getUser().getFirstName() + " "
						+ training.getTrainer().getUser().getLastName())
				.build();
	}

	public RegistrationResponseDTO toProfileResponseDTO(String username, String password) {
		return RegistrationResponseDTO.builder().username(username).password(password).build();
	}

}


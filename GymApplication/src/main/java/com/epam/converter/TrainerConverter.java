package com.epam.converter;

import com.epam.dto.TraineeDTO;
import com.epam.dto.TrainerProfileResponseDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.dto.TrainerTrainingsResponseDTO;
import com.epam.dto.UpdateTrainerProfileDTO;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.TrainingType;
import com.epam.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TrainerConverter {

    public TrainerProfileResponseDTO toProfileResponseDTO(Trainer trainer) {
        List<TraineeDTO> trainees = trainer.getTraineeList().stream()
                .map(this::mapToTraineeDTO).toList();
        return TrainerProfileResponseDTO.builder()
                .firstName(trainer.getUser().getFirstName())
                .lastName(trainer.getUser().getLastName())
                .isActive(trainer.getUser().isActive())
                .specialization(trainer.getSpecialization().getTrainingTypeName())
                .trainees(trainees)
                .build();
    }

    public TrainerProfileUpdateResponseDTO toProfileUpdateResponseDTO(Trainer trainer, UpdateTrainerProfileDTO updateDto) {
        List<TraineeDTO> trainees = trainer.getTraineeList().stream()
                .map(this::mapToTraineeDTO).toList();
        return TrainerProfileUpdateResponseDTO.builder()
                .username(updateDto.getUsername())
                .firstName(updateDto.getFirstName())
                .lastName(updateDto.getLastName())
                .isActive(updateDto.isActive())
                .specialization(trainer.getSpecialization().getTrainingTypeName())
                .trainees(trainees)
                .build();
    }

    public TraineeDTO mapToTraineeDTO(Trainee trainee) {
        TraineeDTO traineeDTO = new TraineeDTO();
        traineeDTO.setUsername(trainee.getUser().getUsername());
        traineeDTO.setFirstName(trainee.getUser().getFirstName());
        traineeDTO.setLastName(trainee.getUser().getLastName());
        return traineeDTO;
    }

    public TrainerTrainingsResponseDTO convertToResponseDTO(Training training) {
        return TrainerTrainingsResponseDTO.builder()
                .trainingName(training.getTrainingName())
                .trainingDate(training.getTrainingDate())
                .trainingType(training.getTrainingType().getTrainingTypeName())
                .trainingDuration(training.getTrainingDuration())
                .traineeName(training.getTrainee().getUser().getFirstName() + " " + training.getTrainee().getUser().getLastName())
                .build();
    }
}

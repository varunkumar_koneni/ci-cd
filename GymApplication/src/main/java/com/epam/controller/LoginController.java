package com.epam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.exception.LoginException;
import com.epam.service.LoginService;

@RestController
@RequestMapping("gymapp")
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@PostMapping("/login")
	public ResponseEntity<Object> loginUser(@RequestParam String username, @RequestParam String password) throws LoginException{
		loginService.loginUser(username,password);	
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping("/login/update")
	public ResponseEntity<Object> updatePassword(@RequestParam String username, @RequestParam String oldPassword,@RequestParam String newPassword) throws LoginException{
		loginService.updatePassword(username,oldPassword,newPassword);	
		return new ResponseEntity<>(HttpStatus.OK);
	}

}

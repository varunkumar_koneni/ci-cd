package com.epam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.model.Trainee;
import com.epam.model.Training;


@Repository
public interface TrainingRepository extends JpaRepository<Training, Integer>{


	List<Training> findByTrainerId(int id);

	List<Training> findByTraineeId(int id);

	void deleteByTrainee(Trainee trainee);



}

package com.epam.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.model.TrainingsSummaryDuration;

@FeignClient(name="report-server",url="http://localhost:8082/")
public interface ReportProxy {
	
    @PostMapping("/trainings")
    public ResponseEntity<TrainingsSummaryDuration> createTrainingsSummaryDuration(@RequestBody TrainingsSummaryDuration trainingsSummaryDuration);
    
    @GetMapping("/trainings")
    public ResponseEntity<List<TrainingsSummaryDuration>> getAllTrainingsSummaryDuration();

}

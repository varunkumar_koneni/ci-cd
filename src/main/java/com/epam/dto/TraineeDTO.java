package com.epam.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TraineeDTO {
	private String username;
    private String firstName;
    private String lastName;    

}

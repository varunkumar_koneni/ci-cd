package com.epam.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TrainerProfileUpdateResponseDTO {
	private String username;
	private String firstName;
    private String lastName;
    private String specialization;
    private boolean isActive;
    private List<TraineeDTO> trainees;

}

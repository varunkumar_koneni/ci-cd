package com.epam.dto;

import java.time.LocalDate;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TraineeRegistrationRequestDTO {

    @NotBlank(message = "First name must not be blank")
    @Size(max = 20, message = "First name must be at most 20 characters long")
    private String firstName;

    @NotBlank(message = "Last name must not be blank")
    @Size(max = 20, message = "Last name must be at most 20 characters long")
    private String lastName;

    @Past(message = "Date of birth must be in the past")
    private LocalDate dateOfBirth;

    @NotBlank(message = "Address must not be blank")
    @Size(max = 100, message = "Address must be at most 100 characters long")
    private String address;

    @NotBlank(message = "Email must not be blank")
    @Email(message = "Email should be valid")
    private String email;

}

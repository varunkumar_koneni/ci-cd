package com.epam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.model.Trainer;

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Integer>{

	Trainer findByUserId(int i);

	Trainer findByUserUsername(String trainerUsername);

}

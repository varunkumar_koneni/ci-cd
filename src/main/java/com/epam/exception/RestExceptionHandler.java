package com.epam.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.model.ExceptionResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;


@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Object> handleIfValuesNull(MethodArgumentNotValidException ex, WebRequest request) {
		List<String> errors = new ArrayList<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String errorMessage = error.getDefaultMessage();
			errors.add(errorMessage);
		});
		ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
				HttpStatus.BAD_REQUEST.name(), errors.toString(), request.getDescription(false));
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionResponse);
	}

	@ExceptionHandler(LoginException.class)
	public ResponseEntity<Object> handleLoginException(LoginException ex, WebRequest request) {
	    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
	    		HttpStatus.BAD_REQUEST.name(), ex.getMessage(), request.getDescription(false));
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionResponse);
	}
	@ExceptionHandler(TraineeNotFoundException.class)
	public ResponseEntity<Object> handleTraineeNotFoundException(TraineeNotFoundException ex, WebRequest request) {
	    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
	    		HttpStatus.BAD_REQUEST.name(), ex.getMessage(), request.getDescription(false));
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionResponse);
	}
	@ExceptionHandler(TrainerNotFoundException.class)
	public ResponseEntity<Object> handleTrainerNotFoundException(TrainerNotFoundException ex, WebRequest request) {
	    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date().toString(),
	    		HttpStatus.BAD_REQUEST.name(), ex.getMessage(), request.getDescription(false));
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exceptionResponse);
	}
	
	

}

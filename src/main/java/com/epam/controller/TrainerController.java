package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TrainerProfileResponseDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.dto.TrainerRegistrationRequestDTO;
import com.epam.dto.TrainerTrainingsResponseDTO;
import com.epam.dto.UpdateTrainerProfileDTO;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.EmailRequest;
import com.epam.proxy.EmailProxy;
import com.epam.service.TrainerService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("gymapp/trainer")
@Slf4j
public class TrainerController {

    @Autowired
    private TrainerService trainerService;
    

    @PostMapping("/register")
    public ResponseEntity<RegistrationResponseDTO> registerTrainer(@RequestBody @Valid TrainerRegistrationRequestDTO request) {
        log.info("Received a request to register a trainer.");
        RegistrationResponseDTO response = trainerService.registerTrainer(request);
        log.info("Trainer registration successful for username: {}", response.getUsername());
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{username}")
    public ResponseEntity<TrainerProfileResponseDTO> getTrainerProfile(@PathVariable  String username) throws TrainerNotFoundException {
        log.info("Received a request to get the profile of trainer with username: {}", username);
        TrainerProfileResponseDTO response = trainerService.getTrainerProfile(username);
        log.info("Retrieved the profile of trainer with username: {}", username);
        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<TrainerProfileUpdateResponseDTO> updateTrainerProfile(@RequestBody UpdateTrainerProfileDTO updateTrainerProfileDTO) throws TrainerNotFoundException {
        log.info("Received a request to update the profile of trainer with username: {}", updateTrainerProfileDTO.getUsername());
        TrainerProfileUpdateResponseDTO response = trainerService.updateTrainerProfile(updateTrainerProfileDTO);
        log.info("Updated the profile of trainer with username: {}", updateTrainerProfileDTO.getUsername());
        return ResponseEntity.ok(response);
    }


    @GetMapping("/{username}/trainings")
    public ResponseEntity<List<TrainerTrainingsResponseDTO>> getTrainerTrainingsList(@PathVariable String username) throws TrainerNotFoundException {
        log.info("Received a request to get the training list for trainer with username: {}", username);
        List<TrainerTrainingsResponseDTO> response = trainerService.getTrainerTrainingsList(username);
        log.info("Retrieved the training list for trainer with username: {}", username);
        return ResponseEntity.ok(response);
    }
}

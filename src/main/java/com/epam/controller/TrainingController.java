package com.epam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AddTrainingRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.exception.TrainerNotFoundException;
import com.epam.service.TrainingServiceImpl;



@RestController
@RequestMapping("gymapp/trainings")
public class TrainingController {
    @Autowired
    private TrainingServiceImpl trainingService;

    @PostMapping("/add")
    public ResponseEntity<Object> addTraining(@RequestBody AddTrainingRequestDTO request) throws TrainerNotFoundException, TraineeNotFoundException {
    	trainingService.addTraining(request);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}

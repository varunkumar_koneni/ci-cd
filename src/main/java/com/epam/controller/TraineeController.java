package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TraineeProfileResponseDTO;
import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TraineeRegistrationRequestDTO;
import com.epam.dto.TraineeTrainingsResponseDTO;
import com.epam.dto.TrainerDTO;
import com.epam.dto.UpdateTraineeProfileDTO;
import com.epam.dto.UpdateTrainerListRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.service.TraineeService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("gymapp/trainees")
public class TraineeController {
	
	
	@Autowired
    private TraineeService traineeService;
	


    @PostMapping("/register")
    public ResponseEntity<RegistrationResponseDTO> registerTrainee(@RequestBody @Valid TraineeRegistrationRequestDTO request) {
        log.info("Received a request to register a trainee.");
        RegistrationResponseDTO response = traineeService.registerTrainee(request);
        log.info("Trainee registration successful for username: {}", response.getUsername());
        return ResponseEntity.ok(response);
    }
    
    @GetMapping("/{username}")
    public ResponseEntity<TraineeProfileResponseDTO> getTraineeProfile(@PathVariable String username) throws TraineeNotFoundException {
        log.info("Received a request to get the profile of trainee with username: {}", username);
        TraineeProfileResponseDTO response = traineeService.getTraineeProfile(username);
        log.info("Retrieved the profile of trainee with username: {}", username);
        return ResponseEntity.ok(response);
    }
    
    @PutMapping
    public ResponseEntity<TraineeProfileUpdateResponseDTO> updateTraineeProfile(@RequestBody UpdateTraineeProfileDTO updateTraineeProfileDTO) throws TraineeNotFoundException {
    	log.info("Received a request to update the profile of trainee with username: {}", updateTraineeProfileDTO.getUsername());
    	TraineeProfileUpdateResponseDTO response = traineeService.updateTraineeProfile(updateTraineeProfileDTO);
    	log.info("Updated the profile of trainee with username: {}", updateTraineeProfileDTO.getUsername());
        return ResponseEntity.ok(response);
    }
    
    @DeleteMapping("/{username}")
    public ResponseEntity<Void> deleteTraineeProfile(@PathVariable String username) throws TraineeNotFoundException {
    	log.info("Received a request to delete the profile of trainee with username: {}", username);
        traineeService.deleteTraineeProfile(username);
        log.info("Deleted the profile of trainee with username: {}", username);
		return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @GetMapping("/not-assigned-trainers")
    public ResponseEntity<List<TrainerDTO>> getNotAssignedActiveTrainers(@RequestParam String username) throws TraineeNotFoundException {
        log.info("Received a request to get not assigned active trainers for trainee with username: {}", username);
        List<TrainerDTO> response = traineeService.getNotAssignedActiveTrainers(username);
        log.info("Retrieved not assigned active trainers for trainee with username: {}", username);
        return ResponseEntity.ok(response);
    }
    
    @GetMapping("/trainingsList")
    public ResponseEntity<List<TraineeTrainingsResponseDTO>> getTraineeTrainingsList(@RequestParam String username) throws TraineeNotFoundException {
        log.info("Received a request to get the training list for trainee with username: {}", username);
        List<TraineeTrainingsResponseDTO> response = traineeService.getTraineeTrainingsList(username);
        log.info("Retrieved the training list for trainee with username: {}", username);
        return ResponseEntity.ok(response);
    }
    
    @PutMapping("/update-trainers")
    public ResponseEntity<List<TrainerDTO>> updateTraineeTrainersList(@RequestBody UpdateTrainerListRequestDTO request) throws TraineeNotFoundException {
    	log.info("Received a request to update trainers list for trainee with username: {}", request.getTraineeUsername());
    	List<TrainerDTO> response = traineeService.updateTraineeTrainersList(request);
    	log.info("Updated trainers list for trainee with username: {}", request.getTraineeUsername());
        return ResponseEntity.ok(response);
    }
}

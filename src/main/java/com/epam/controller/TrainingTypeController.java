package com.epam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.TrainingTypeDTO;
import com.epam.service.TrainingTypeService;

@RestController
@RequestMapping("gymapp")
public class TrainingTypeController {

	@Autowired
    TrainingTypeService trainingTypeService;


    @GetMapping("/training-types")
    public ResponseEntity<List<TrainingTypeDTO>> getAllTrainingTypes() {
        List<TrainingTypeDTO> trainingTypes = trainingTypeService.getAllTrainingTypes();
        return ResponseEntity.ok(trainingTypes);
    }
}


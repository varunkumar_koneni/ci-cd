package com.epam.model;

import java.util.List;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TrainingsSummaryDuration {
	
    private String trainerUsername;
    private String trainerFirstName;
    private String trainerLastName;
    private String traineeStatus; 
    private List<Integer> yearsList;
    private List<String> monthsList;

}

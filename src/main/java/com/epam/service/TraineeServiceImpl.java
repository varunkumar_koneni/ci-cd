package com.epam.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.converter.TraineeConverter;
import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TraineeProfileResponseDTO;
import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TraineeRegistrationRequestDTO;
import com.epam.dto.TraineeTrainingsResponseDTO;
import com.epam.dto.TrainerDTO;
import com.epam.dto.UpdateTraineeProfileDTO;
import com.epam.dto.UpdateTrainerListRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.User;
import com.epam.repository.TraineeRepository;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TraineeServiceImpl implements TraineeService{

	@Autowired
	public UserRepository userRepository;

	@Autowired
	public TraineeRepository traineeRepository;

	@Autowired
	TrainerRepository trainerRepository;

	@Autowired
	RegistrationResponseDTO response;

	@Autowired
	TraineeProfileUpdateResponseDTO traineeProfileUpdateResponseDTO;

	@Autowired
	TrainingRepository trainingRepository;

	@Autowired
	EmailService emailService;

	@Autowired
	TraineeConverter traineeConverter;
	
    @Autowired
    private PasswordEncoder passwordEncoder;

	@Override
	public RegistrationResponseDTO registerTrainee(TraineeRegistrationRequestDTO request) {

		String username = request.getEmail().split("@")[0];
		String password = "12345";
		
		User user = User.builder().firstName(request.getFirstName()).lastName(request.getLastName()).isActive(true)
				.email(request.getEmail()).username(username).password(passwordEncoder.encode(password)).build();
		userRepository.save(user);
		Trainee trainee = Trainee.builder().user(user).dateOfBirth(request.getDateOfBirth())
				.address(request.getAddress()).build();
		traineeRepository.save(trainee);
		emailService.sendRegistrationEmailToTrainee(username, password, request.getEmail());

		RegistrationResponseDTO response = traineeConverter.toProfileResponseDTO(username,password);
		
		return response;
	}

	@Override
	public TraineeProfileResponseDTO getTraineeProfile(String username) throws TraineeNotFoundException {
		log.info("Received a request to get the profile of trainee with username: {}", username);
		Trainee trainee = getTraineeByUsername(username);
		TraineeProfileResponseDTO responseDTO = traineeConverter.toProfileResponseDTO(trainee);
		log.info("Retrieved the profile of trainee with username: {}", username);
		return responseDTO;
	}

	@Override
	public TraineeProfileUpdateResponseDTO updateTraineeProfile(UpdateTraineeProfileDTO updateTraineeProfileDTO)
			throws TraineeNotFoundException {

		log.info("Received a request to update the profile of trainee with username: {}",
				updateTraineeProfileDTO.getUsername());
		Trainee trainee = getTraineeByUsername(updateTraineeProfileDTO.getUsername());
		trainee.getUser().setFirstName(updateTraineeProfileDTO.getFirstName());
		trainee.getUser().setLastName(updateTraineeProfileDTO.getLastName());
		trainee.getUser().setActive(updateTraineeProfileDTO.isActive());
		trainee.setAddress(updateTraineeProfileDTO.getAddress());
		trainee.setDateOfBirth(updateTraineeProfileDTO.getDateOfBirth());

		traineeRepository.save(trainee);
		log.info("Updated the profile of trainee with username: {}", updateTraineeProfileDTO.getUsername());
		TraineeProfileUpdateResponseDTO responseDTO = traineeConverter.toProfileUpdateResponseDTO(trainee,
				updateTraineeProfileDTO);
		emailService.sendProfileUpdatedEmailToTrainee(trainee.getUser().getEmail(), traineeProfileUpdateResponseDTO);

		return responseDTO;
	}
	
	@Override
	public void deleteTraineeProfile(String username) throws TraineeNotFoundException {
		log.info("Received a request to delete the profile of trainee with username: {}", username);
		Trainee trainee = getTraineeByUsername(username);
		traineeRepository.delete(trainee);
		userRepository.delete(trainee.getUser());
		log.info("Deleted the profile of trainee with username: {}", username);
	}
	
	@Override
	public List<TrainerDTO> getNotAssignedActiveTrainers(String username) throws TraineeNotFoundException {
		log.info("Received a request to get not assigned active trainers for trainee with username: {}", username);
		Trainee trainee = getTraineeByUsername(username);
		List<Trainer> assignedTrainers = trainee.getTrainerList();
		log.info("Retrieved not assigned active trainers for trainee with username: {}", username);

		return trainerRepository.findAll().stream()
				.filter(trainer -> !assignedTrainers.contains(trainer) && trainer.getUser().isActive())
				.map(traineeConverter::mapToTrainerDTO).toList();
	}
	
	@Override
	public List<TraineeTrainingsResponseDTO> getTraineeTrainingsList(String username) throws TraineeNotFoundException {
		log.info("Received a request to get the training list for trainee with username: {}", username);
		Trainee trainee = getTraineeByUsername(username);
		List<Training> traineeTrainings = trainingRepository.findByTraineeId(trainee.getId());
		log.info("Retrieved the training list for trainee with username: {}", username);

		return traineeTrainings.stream().map(traineeConverter::convertToResponseDTO).toList();
	}
	
	@Override
	public List<TrainerDTO> updateTraineeTrainersList(UpdateTrainerListRequestDTO request)
			throws TraineeNotFoundException, TrainerNotFoundException {
		log.info("Updating trainers list for trainee with username: {}", request.getTraineeUsername());
		Trainee trainee = getTraineeByUsername(request.getTraineeUsername());
		List<Trainer> currentTrainers = trainee.getTrainerList();
		
		List<Trainer> trainersList = request.getTrainersList().stream()
		        .map(trainerDto -> {
		            Trainer trainer = trainerRepository.findByUserUsername(trainerDto);
		            if (trainer == null) {
		                throw new TrainerNotFoundException("Trainer not found");
		            }
		            return trainer;
		        })
                .filter(trainer -> !currentTrainers.contains(trainer))
				.toList();
				
		currentTrainers.addAll(trainersList);
		trainee.setTrainerList(currentTrainers);
		traineeRepository.save(trainee);

		log.info("Updated trainers list for trainee with username: {}", request.getTraineeUsername());
		return trainersList.stream().map(traineeConverter::mapToTrainerDTO).toList();
	}

	private Trainee getTraineeByUsername(String username) throws TraineeNotFoundException {
		return Optional.ofNullable(traineeRepository.findByUserUsername(username))
				.orElseThrow(() -> new TraineeNotFoundException("User not found"));
	}
}

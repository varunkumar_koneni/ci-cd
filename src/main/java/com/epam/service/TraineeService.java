package com.epam.service;

import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TraineeProfileResponseDTO;
import com.epam.dto.TraineeProfileUpdateResponseDTO;
import com.epam.dto.TraineeRegistrationRequestDTO;
import com.epam.dto.TraineeTrainingsResponseDTO;
import com.epam.dto.TrainerDTO;
import com.epam.dto.UpdateTraineeProfileDTO;
import com.epam.dto.UpdateTrainerListRequestDTO;
import com.epam.exception.TraineeNotFoundException;
import com.epam.exception.TrainerNotFoundException;

import java.util.List;

public interface TraineeService {

    RegistrationResponseDTO registerTrainee(TraineeRegistrationRequestDTO request);

    TraineeProfileResponseDTO getTraineeProfile(String username) throws TraineeNotFoundException;

    TraineeProfileUpdateResponseDTO updateTraineeProfile(UpdateTraineeProfileDTO updateTraineeProfileDTO)
            throws TraineeNotFoundException;

    void deleteTraineeProfile(String username) throws TraineeNotFoundException;

    List<TrainerDTO> getNotAssignedActiveTrainers(String username) throws TraineeNotFoundException;

    List<TraineeTrainingsResponseDTO> getTraineeTrainingsList(String username) throws TraineeNotFoundException;

    List<TrainerDTO> updateTraineeTrainersList(UpdateTrainerListRequestDTO request)
            throws TraineeNotFoundException, TrainerNotFoundException;
}

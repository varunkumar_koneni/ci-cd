package com.epam.service;

import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TrainerProfileResponseDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.dto.TrainerRegistrationRequestDTO;
import com.epam.dto.TrainerTrainingsResponseDTO;
import com.epam.dto.UpdateTrainerProfileDTO;
import com.epam.exception.TrainerNotFoundException;

import java.util.List;

public interface TrainerService {

    RegistrationResponseDTO registerTrainer(TrainerRegistrationRequestDTO request);

    TrainerProfileResponseDTO getTrainerProfile(String username) throws TrainerNotFoundException;

    TrainerProfileUpdateResponseDTO updateTrainerProfile(UpdateTrainerProfileDTO updateTrainerProfileDTO)
            throws TrainerNotFoundException;
    
    List<TrainerTrainingsResponseDTO> getTrainerTrainingsList(String username) throws TrainerNotFoundException;

}

package com.epam.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.epam.dto.AddTrainingRequestDTO;
import com.epam.dto.TrainerReportDto;
import com.epam.exception.TraineeNotFoundException;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.EmailRequest;
import com.epam.model.Trainee;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.TrainingType;
import com.epam.model.User;
import com.epam.repository.TraineeRepository;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.TrainingTypeRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TrainingServiceImpl implements TrainingService {

	
	@Autowired
	TraineeRepository traineeRepository;
	
	@Autowired
	TrainerRepository trainerRepository;
	
	@Autowired
	TrainingRepository trainingRepository;
	
	
	@Autowired
	TrainingTypeRepository trainingTypeRepository;
	
	@Autowired
	Training training;
	
	@Autowired
	EmailService emailService;
	

    @Autowired
    private KafkaTemplate<String, EmailRequest> kafkaReportTemplate;

    private static final String REPORT_TOPIC = "report";

    @Override
	public void addTraining(AddTrainingRequestDTO request) throws TrainerNotFoundException, TraineeNotFoundException {
        log.info("Received a request to add training: Trainee - {}, Trainer - {}, Training - {}",
                request.getTraineeUsername(), request.getTrainerUsername(), request.getTrainingName());
		
		 Trainee trainee = getTraineeByUsername(request.getTraineeUsername());
	     Trainer trainer = getTrainerByUsername(request.getTrainerUsername());
	     
	     trainee.getTrainerList().add(trainer);
	     traineeRepository.save(trainee);
	    	Training training = Training.builder()
	                .trainee(trainee)
	                .trainingName(request.getTrainingName())
	                .trainingDate(request.getTrainingDate())
	                .trainingType(trainer.getSpecialization())
	                .trainingDuration(request.getTrainingDuration())
	                .trainer(trainer) 
	                .build();
	     
	     trainingRepository.save(training);
	     
	     emailService.sendTrainingConfirmationEmailToTrainee(trainee.getUser().getEmail(),training);
	     emailService.sendTrainingConfirmationEmailToTrainer(trainer.getUser().getEmail(),training);
	     
	     TrainerReportDto trainerReportDto= TrainerReportDto.builder().trainerUsername(trainer.getUser().getUsername())
	     							.firstName(trainer.getUser().getFirstName())
	     							.lastName(trainer.getUser().getLastName())
	     							.isActive(trainer.getUser().isActive())
	     							.trainingDate(request.getTrainingDate())
	     							.trainingDuration(request.getTrainingDuration()).build();
	     
	        Message<TrainerReportDto> report = MessageBuilder.withPayload(trainerReportDto)
	                .setHeader(KafkaHeaders.TOPIC, REPORT_TOPIC).build();

	        kafkaReportTemplate.send(report);
	     									
	}
    
	private Trainee getTraineeByUsername(String username) throws TraineeNotFoundException {
		return Optional.ofNullable(traineeRepository.findByUserUsername(username))
				.orElseThrow(() -> new TraineeNotFoundException("User not found"));
	}
	private Trainer getTrainerByUsername(String username) throws TrainerNotFoundException {
		return Optional.ofNullable(trainerRepository.findByUserUsername(username))
				.orElseThrow(() -> new TrainerNotFoundException("User not found"));
	}

}

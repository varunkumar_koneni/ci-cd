package com.epam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epam.converter.TrainerConverter;
import com.epam.dto.RegistrationResponseDTO;
import com.epam.dto.TrainerProfileResponseDTO;
import com.epam.dto.TrainerProfileUpdateResponseDTO;
import com.epam.dto.TrainerRegistrationRequestDTO;
import com.epam.dto.TrainerTrainingsResponseDTO;
import com.epam.dto.UpdateTrainerProfileDTO;
import com.epam.exception.TrainerNotFoundException;
import com.epam.model.Trainer;
import com.epam.model.Training;
import com.epam.model.TrainingType;
import com.epam.model.User;
import com.epam.repository.TrainerRepository;
import com.epam.repository.TrainingRepository;
import com.epam.repository.TrainingTypeRepository;
import com.epam.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TrainerServiceImpl implements TrainerService {

	@Autowired
	private TrainerRepository trainerRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TrainingRepository trainingRepository;

	@Autowired
	private TrainerConverter trainerConverter;

	@Autowired
	private EmailService emailService;


	@Autowired
	private TrainingTypeRepository trainingTypeRepository;

	@Override
	public RegistrationResponseDTO registerTrainer(TrainerRegistrationRequestDTO request) {
		log.info("Received a request to register a new trainer: {}", request.getEmail());
		String username = request.getEmail().split("@")[0];
		String password = request.getFirstName().toLowerCase() + "123";

		User user = User.builder().firstName(request.getFirstName()).lastName(request.getLastName()).isActive(true)
				.email(request.getEmail()).username(username).password(password).build();

		user = userRepository.save(user);
		
	    TrainingType trainingType = trainingTypeRepository.findByTrainingTypeName(request.getSpecialization());

		Trainer trainer = Trainer.builder().user(user).specialization(trainingType).build();

		trainerRepository.save(trainer);

		emailService.sendRegistrationEmailToTrainer(username, password, request.getEmail());
		log.info("Trainer registered successfully: {}", request.getEmail());

		return RegistrationResponseDTO.builder().username(username).password(password).build();
		
	}

	@Override
	public TrainerProfileResponseDTO getTrainerProfile(String username) throws TrainerNotFoundException {
		log.info("Received a request to get the profile for trainer: {}", username);
		Trainer trainer = getTrainerByUsername(username);
		return trainerConverter.toProfileResponseDTO(trainer);
	}

	@Override
	public TrainerProfileUpdateResponseDTO updateTrainerProfile(UpdateTrainerProfileDTO updateTrainerProfileDTO)
			throws TrainerNotFoundException {
		
		log.info("Received a request to update the profile for trainer: {}", updateTrainerProfileDTO.getUsername());

		Trainer trainer = getTrainerByUsername(updateTrainerProfileDTO.getUsername());
		trainer.getUser().setFirstName(updateTrainerProfileDTO.getFirstName());
		trainer.getUser().setLastName(updateTrainerProfileDTO.getLastName());
		trainer.getUser().setActive(updateTrainerProfileDTO.isActive());

		trainerRepository.save(trainer);

		TrainerProfileUpdateResponseDTO response = trainerConverter.toProfileUpdateResponseDTO(trainer,
				updateTrainerProfileDTO);
		emailService.sendProfileUpdatedEmailToTrainer(trainer.getUser().getEmail(), response);


		log.info("Profile updated successfully for trainer: {}", updateTrainerProfileDTO.getUsername());
		return response;
	}

	@Override
	public List<TrainerTrainingsResponseDTO> getTrainerTrainingsList(String username) throws TrainerNotFoundException {
		log.info("Received a request to get the training list for trainer: {}", username);

		Trainer trainer = getTrainerByUsername(username);
		List<Training> trainerTrainings = trainingRepository.findByTrainerId(trainer.getId());

		log.info("Retrieved the training list for trainer: {}", username);
		return trainerTrainings.stream().map(trainerConverter::convertToResponseDTO).toList();
	}

	private Trainer getTrainerByUsername(String username) throws TrainerNotFoundException {
		return Optional.ofNullable(trainerRepository.findByUserUsername(username))
				.orElseThrow(() -> new TrainerNotFoundException("User not found"));
	}
}

package com.epam.service;

import com.epam.exception.LoginException;

public interface LoginService {

    void loginUser(String username, String password) throws LoginException;

    void updatePassword(String username, String oldPassword, String newPassword) throws LoginException;
}

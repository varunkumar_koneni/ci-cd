package com.epam.testing.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.TraineeController;
import com.epam.controller.TrainingTypeController;
import com.epam.dto.TrainingTypeDTO;
import com.epam.service.TrainingTypeService;

@WebMvcTest(TrainingTypeController.class)
public class TrainingTypeControllerTesting {
	
	@MockBean
    TrainingTypeService trainingTypeService;
	
	@Autowired
	MockMvc mockMvc;
	
    @Test
    public void testGetAllTrainingTypes() throws Exception {
        List<TrainingTypeDTO> response = new ArrayList<>();

        when(trainingTypeService.getAllTrainingTypes()).thenReturn(response);

        mockMvc.perform(get("/gymapp/training-types"))
                .andExpect(status().isOk());

        verify(trainingTypeService).getAllTrainingTypes();
    }

}

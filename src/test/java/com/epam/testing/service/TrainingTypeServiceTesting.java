package com.epam.testing.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.TrainingTypeDTO;
import com.epam.model.TrainingType;
import com.epam.repository.TrainingTypeRepository;
import com.epam.service.TrainingTypeService;

@ExtendWith(MockitoExtension.class)
public class TrainingTypeServiceTesting {

	@InjectMocks
	private TrainingTypeService trainingTypeService;

	@Mock
	private TrainingTypeRepository trainingTypeRepository;

	@Mock
	private ModelMapper modelMapper;

	@Test
	public void testGetAllTrainingTypes() {
		TrainingType trainingType1= TrainingType.builder().trainingTypeName("fitness").build();

		TrainingType trainingType2= TrainingType.builder().trainingTypeName("zumba").build();

		List<TrainingType> trainingTypes = Arrays.asList(trainingType1, trainingType2);

		TrainingTypeDTO trainingTypeDto1= TrainingTypeDTO.builder().trainingTypeName("fitness").build();

		TrainingTypeDTO trainingTypeDto2= TrainingTypeDTO.builder().trainingTypeName("zumba").build();

		when(trainingTypeRepository.findAll()).thenReturn(trainingTypes);
		when(modelMapper.map(trainingType1, TrainingTypeDTO.class)).thenReturn(trainingTypeDto1);
		when(modelMapper.map(trainingType2, TrainingTypeDTO.class)).thenReturn(trainingTypeDto2);

		List<TrainingTypeDTO> result = trainingTypeService.getAllTrainingTypes();

		assertEquals(2, result.size());
		assertEquals("fitness", result.get(0).getTrainingTypeName());
		assertEquals("zumba", result.get(1).getTrainingTypeName());

		verify(trainingTypeRepository).findAll();
		verify(modelMapper).map(trainingType1, TrainingTypeDTO.class);
		verify(modelMapper).map(trainingType2, TrainingTypeDTO.class);
	}

}
